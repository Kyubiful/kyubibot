const Discord = require('discord.js');
const client = new Discord.Client();

const commands = ['v', 'coinflip'];
const prefix = '!';
const mapPool = ['dust2', 'mirage', 'inferno', 'overpass', 'train', 'nuke', 'vertigo'];
var banning = false;
var side = false;
var playerOne = '';
var playerTwo = '';
var teamOne = '';
var teamTwo = '';
var veto = [];
var cont = 0;
var maps = [];

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {

  if(correctCommand(msg, prefix)){ 

    var entryCommand = getCommand(msg);
    var args = getArgs(msg);

    if(args.length == 4 && banning == false){  
      teamOne = args[0];
      teamTwo = args[1];
      playerOne = args[2];
      playerTwo = args[3];
      veto.push(teamOne);
      veto.push(teamTwo);
      maps = mapPool.slice();
    }
    if(
      (entryCommand == '!veto' || entryCommand == '!v') 
      && args.length == 4 
      && userExist(msg, playerOne) 
      && userExist(msg, playerTwo)
      && banning == false
    ){    

      banning = true;
      msg.channel.send(embedMsg(`**${playerOne}**. ¿Quieres banear primero o segundo? 
(!v primero/segundo)`));

    } else if(

      (entryCommand == '!veto' || entryCommand == '!v')
      && msg.author.username == playerOne 
      && (args[0].toLowerCase() == 'primero' || args[0].toLowerCase() == 'segundo')
      && banning == true

    ){

      if(args[0] == 'segundo'){
        let aux = '';
        aux = playerOne;
        playerOne = playerTwo;
        playerTwo = aux;
        aux = teamOne;
        teamOne = teamTwo;
        teamTwo = aux;
      }
      veto.push(teamOne);
      veto.push(teamTwo);
      side = true;
      msg.channel.send(embedMsg(`Maps: ${maps}
${teamOne} **BAN**: (!v 'Mapa')`));

    } else if(
      (entryCommand == '!veto' || entryCommand == '!v')
      && msg.author.username == playerOne
      && banning == true
      && side == true
      && correctMap(msg, args, maps)
      && cont == 0
    ){

      deleteMap(maps, args[0]);
      cont++;
      veto.push(args[0].toLowerCase());
      msg.channel.send(embedMsg(`Maps: ${maps}
${teamTwo} **BAN**: (!v 'Mapa')`));

    } else if(
      (entryCommand == '!veto' || entryCommand == '!v')
      && msg.author.username == playerTwo
      && banning == true
      && side == true
      && correctMap(msg, args, maps)
      && cont == 1
    ){
      deleteMap(maps, args[0]);
      cont++;
      veto.push(args[0].toLowerCase());
      msg.channel.send(embedMsg(`Maps: ${maps}
${teamOne} **PICK**: (!v 'Mapa')`));

    } else if (
      (entryCommand == '!veto' || entryCommand == '!v')
      && msg.author.username == playerOne
      && banning == true
      && side == true
      && correctMap(msg, args, maps)
      && cont == 2
    ){
      deleteMap(maps, args[0]);
      cont++;
      veto.push(args[0].toLowerCase());
      msg.channel.send(embedMsg(`${teamTwo} Elige lado: (!v ct/t)`));
    } else if(
      (entryCommand == '!veto' || entryCommand == '!v')
      && msg.author.username == playerTwo
      && banning == true
      && side == true
      && (args[0].toLowerCase() == 'ct' || args[0].toLowerCase() == 't')
      && cont == 3
    ){
      cont ++;
      veto.push(args[0].toLowerCase());
      msg.channel.send(embedMsg(`Maps: ${maps}
${teamTwo} **PICK**: (!v 'Mapa')`));
    } else if (
      (entryCommand == '!veto' || entryCommand == '!v')
      && msg.author.username == playerTwo
      && banning == true
      && side == true
      && correctMap(msg, args, maps)
      && cont == 4
    ){
      deleteMap(maps, args[0]);
      cont++;
      veto.push(args[0].toLowerCase());
      msg.channel.send(embedMsg(`${teamOne} Elige lado: (!v ct/t)`));
    } else if (
      (entryCommand == '!veto' || entryCommand == '!v')
      && msg.author.username == playerOne
      && banning == true
      && side == true
      && (args[0].toLowerCase() == 'ct' || args[0].toLowerCase() == 't')
      && cont == 5
    ){
      cont ++;
      veto.push(args[0].toLowerCase());
      msg.channel.send(embedMsg(`Maps: ${maps}
${teamOne} **BAN**: (!v 'Mapa')`));
    } else if(
      (entryCommand == '!veto' || entryCommand == '!v')
      && msg.author.username == playerOne
      && banning == true
      && side == true
      && correctMap(msg, args, maps)
      && cont == 6
    ){
      deleteMap(maps, args[0]);
      cont++;
      veto.push(args[0].toLowerCase());
      msg.channel.send(embedMsg(`Maps: ${maps}
${teamTwo} **BAN**: (!v 'Mapa')`));
    } else if(
      (entryCommand == '!veto' || entryCommand == '!v')
      && msg.author.username == playerTwo
      && banning == true
      && side == true
      && correctMap(msg, args, maps)
      && cont == 7
    ){
      deleteMap(maps, args[0]);
      cont++;
      veto.push(args[0].toLowerCase());
      veto.push(maps[0]);

      let finalVeto =`
${veto[2]} BAN: ${veto[4]}
${veto[3]} BAN: ${veto[5]}
${veto[2]} PICK: **${veto[6]}**, ${veto[7]} para ${veto[3]}
${veto[3]} PICK: **${veto[8]}**, ${veto[9]} para ${veto[2]}
${veto[2]} BAN: ${veto[10]}
${veto[3]} BAN: ${veto[11]}

DECIDER: **${veto[12]}**

**- ${veto[0]} vs ${veto[1]}** -de_${veto[6]} -de_${veto[8]} -de_${veto[12]}`;
      msg.channel.send(embedGrandMsg(`${veto[0]} vs ${veto[1]}`, '\u200B', finalVeto));

      msg.channel.send(`
¡¡Muchas gracias!!`);
      msg.guild.channels.cache.get('771569436368568352').send(embedGrandMsg(`${veto[0]} vs ${veto[1]}`, '\u200B', finalVeto));
      banning = false;
      side = false;
      cont = 0;
      maps = mapPool.slice();
      veto = [];
    }
    if(entryCommand == '!v' && args[0] == 'reset'){
      banning = false;
      side = false;
      cont = 0;
      maps = mapPool.slice();
      veto = [];

      msg.channel.send(embedGrandMsg('ESL Masters', 'Reset Completed', '\u200B'));
    }

    if(entryCommand === '!coinflip'){
      msg.channel.send(embedGrandMsg('ESL Masters', coinFlip().toUpperCase(), '\u200B'));
    }
  }
});

const coinFlip = () => {
  return (Math.floor(Math.random()*2) == 0) ? 'cara' : 'cruz';
}

const correctCommand = (msg, prefix) => {
  let cmd = getCommand(msg);
  for(command in commands){
    if(cmd == prefix+commands[command]){
      return true;
    } 
  }
  return false;
}

const getCommand = (msg) => {
  let command = msg.content.toLowerCase().split(' ')[0];
  return command;
}

const getArgs = (msg) => {
  let args = msg.content.split(' ');
  args.splice(0,1);
  return args;
}

const userExist = (msg, name) => {
  var mem = msg.guild.members.cache;
  for (var [key] of mem){
    if(mem.get(key).user.username == name){
      return true;
    }
  }
  return false;
  
}

const correctMap = (msg, args, maps) => {
  for(map in maps){
    if(args[0] == maps[map]){
      return true;
    }
  }
  return false;
}

const deleteMap = (maps, map) => {
  for(m in maps){
    if(maps[m] == map){
      maps.splice(m, 1);
    }
  }
  return maps;
}

const embedGrandMsg = (title, subtitle, msg) => {
  const embed = new Discord.MessageEmbed()
    .setColor('#B21901')
	  .setTitle(title)
	  .setThumbnail('https://cdn-assets-cloud.frontify.com/s3/frontify-cloud-files-us/eyJwYXRoIjoiZnJvbnRpZnlcL2FjY291bnRzXC82ZFwvMTQyOTExXC9wcm9qZWN0c1wvMTc5OTY0XC9hc3NldHNcLzRiXC8yOTkxMjk4XC85ODFlYjNlMzkwOGYzY2VlYjI3M2JiYzc4YmMxYmM0MS0xNTQ4OTMxNTMzLnBuZyJ9:cloud:xSFDQmGAZOxooRextbUOrZGVXiJHBcKWxq0I8p_xbkI?width=719')
	  .addFields(
		  { name: subtitle, value: msg },
	  )
	  .setTimestamp()
	  .setFooter('KyubiBot', 'https://cdn-assets-cloud.frontify.com/s3/frontify-cloud-files-us/eyJwYXRoIjoiZnJvbnRpZnlcL2FjY291bnRzXC82ZFwvMTQyOTExXC9wcm9qZWN0c1wvMTc5OTY0XC9hc3NldHNcLzRiXC8yOTkxMjk4XC85ODFlYjNlMzkwOGYzY2VlYjI3M2JiYzc4YmMxYmM0MS0xNTQ4OTMxNTMzLnBuZyJ9:cloud:xSFDQmGAZOxooRextbUOrZGVXiJHBcKWxq0I8p_xbkI?width=719');

  return embed; 
}

const embedMsg = (msg) => {
  const embed = new Discord.MessageEmbed()
    .setColor('#B21901')
	  .setThumbnail('https://cdn-assets-cloud.frontify.com/s3/frontify-cloud-files-us/eyJwYXRoIjoiZnJvbnRpZnlcL2FjY291bnRzXC82ZFwvMTQyOTExXC9wcm9qZWN0c1wvMTc5OTY0XC9hc3NldHNcLzRiXC8yOTkxMjk4XC85ODFlYjNlMzkwOGYzY2VlYjI3M2JiYzc4YmMxYmM0MS0xNTQ4OTMxNTMzLnBuZyJ9:cloud:xSFDQmGAZOxooRextbUOrZGVXiJHBcKWxq0I8p_xbkI?width=719')
	  .addFields(
		  { name: '\u200B', value: msg },
	  )

  return embed; 
}

client.login(secret.token);
